from tickstore.query import QuerySettings, QueryFunctor
import unittest
from tickstore import db, client
import time
import os


class TestClient(unittest.TestCase):
    def test_write_query(self):
        """
        Test delta buffer
        """

        if os.environ.get('https_proxy'):
            del os.environ['https_proxy']
        if os.environ.get('http_proxy'):
            del os.environ['http_proxy']

        c = client.Client("127.0.0.1:4541")
        tags = {
            "id": "1",
        }

        c.register_measurement("counter", "UInt64")

        start_ts = round(time.time_ns() / 1000000)
        writer = c.new_tick_writer("counter", tags)

        counter = db.UInt64(10)

        ts = start_ts
        for i in range(100):
            counter.value = i
            writer.write_object(ts, counter)
            ts += 1

        writer.close()

        qs = QuerySettings(
            streaming=False,
            batch_size=1000,
            timeout=1000000,
            selector='SELECT counter WHERE id="1"',
            frm=start_ts,
            to=ts
        )
        q = c.new_query(qs)

        i = 0
        while q.next():
            tick, obj, tags = q.read()
            self.assertEqual(obj.value, i); i += 1

        #TODO fix
        #self.assertIs(q.err(), EOFError)
        q.close()

    def test_obprice(self):
        """
        Test delta buffer
        """

        if os.environ.get('https_proxy'):
            del os.environ['https_proxy']
        if os.environ.get('http_proxy'):
            del os.environ['http_proxy']

        c = client.Client("127.0.0.1:4541")
        c.register_measurement("orderbook", "RawOrderBook")

        tags = {
            "type": "SPOT",
            "exchange": "bitstamp",
            "base": "BTC",
            "quote": "USD"
        }

        qs = QuerySettings(
            streaming=False,
            batch_size=1000,
            timeout=1000000,
            selector='SELECT OBPrice(orderbook, "0.01") WHERE id="1"',
            frm=1571830000000,
            to=1571839000000,
        )
        q = c.new_query(qs)

        i = 0
        while q.next():
            tick, obj, tags = q.read()
            print(tick, obj.price)

        #TODO fix
        #self.assertIs(q.err(), EOFError)
        print(q.err())
        q.close()

if __name__ == '__main__':
    unittest.main()
